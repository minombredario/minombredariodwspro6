<?php

    //require("configBD.php");
    
    class ConectarServidor{
        
        public static function conectar(){
            $dbhost = configBD::$bdhostname;
            $dbuser = configBD::$bdusuario;
            $dbpass = configBD::$bdclave;
           try{
               
               $bdconexion = new PDO('mysql:host=' . $dbhost, $dbuser, $dbpass);
               $bdconexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
               $bdconexion->exec("SET CHARACTER SET utf8");
              
               return $bdconexion;
               
            } catch (Exception $e) {

                die ('Error: ' . $e->getMessage());

                echo "la línea de error es: " . $e->getLine();//devuelve la linea donde esta el error
            }
        }
    }
    
  
?>

          